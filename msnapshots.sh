#!/bin/bash
# 
# ***** UBUNTU NOTE *****
# Ubuntu 10.10 and newer has a new default security policy that affects
# strace, jmap, jinfo, and other Serviceability commands.
#
# This policy prevents a process from attaching to another process owned
# by the same UID if the target process is not a descendant of the 
# attaching process.
# 
# The following will change the kernel's yama/ptrace_scope variable
# temporarily, i.e., until the next reboot:
#
#  $ echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
# 
# For more info see: 
# http://bugs.java.com/bugdatabase/view_bug.do?bug_id=7050524
# 
# Set value to 1 on the next line if you'd like to script to set this for you.
PTRACE_SCOPE=0
#
#
#  What to do with this script:
#
#  1. Start the target JVM to include these options:
#     -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps
#     -XX:+PrintGCApplicationStoppedTime
#     -XX:+PrintGCApplicationConcurrentTime
#     -Xloggc:/path/to/gclogfile   [Replace with your local file path]
#
#     You will have to independently collect the "gclogfile"
#     and send it to us.
#
#  2. Run the script (with JVM's pid as a parameter) to take snapshots of data
#     You should modify the "outlib=/tmp" line below to
#     specify a directory for storing the output files.
#     Modify the other configuration parameters as necessary.
#
#     Collect the gzipped tarball that is produced.
#
#  3. Collect the stdout log, which contains the "kill -3" output
#     Typically this is redirected to an error log (container error log)
#     Such as your tomcat error log. You'll need to collect this 
#     independently as well, similar to your gclogfile.
#
#  Notes: 
#  1. Please make sure JDKHOME/bin is in your execution path
#     so that this script can use jmap, jstack etc
#  2. Make sure you run it once in a testing scenario,
#     just to make sure you get all the output.     

if [[ $# -eq 0  ]]; then
          echo "usage: msnapshots.sh <PID>"
          exit  0
fi

# Configuration Parameters below - change what is needed!

outlib=/tmp/$1         	# change this to a location you want files stored 

if [ ! -d "$outlib" ]; then # if the directory doesn't exist, we'll create it
mkdir $outlib           # Make sure you set outlib to a good location 
fi

opt=                   	# options such as -d64 if needed can go here
LOOP_COUNT=3      	# take "LOOP_COUNT" snapshots of data
SLEEP_INTERVAL=0       	# sleep "SLEEP_INTERVAL" sec interval within the loop
DO_JINFO=1	       	# turn on/off jinfo
DO_GCORE_BEFORE_LOOP=0	# turn on/off gcore before entering loop (0=Off)
DO_GCORE_AFTER_LOOP=0  	# turn on/off gcore after exiting loop      (0=Off)
DO_HEAPDUMP_AFTER_LOOP=1  # turn on/off heapdump after exiting loop

DO_PRSTAT=0            	# turn on/off prstat  (Solaris)
DO_TOP=1	      	# turn on/off top -n 1 -H -p <pid> /*M*/
DO_PSTACK=0           	# turn on/off pstack (Solaris)
DO_JSTACK=1           	# turn on/off jstack -m
DO_KILL3=1             	# turn on/off kill -3 for 
                       # - java threads stack trace
                       # - class histogram with -XX:+PrintClassHistogram
                       
DO_PMAP=1              	# turn on/off  pmap -x /*M*/
DO_HEAPSUM=1            # turn on/off heapsum (uses jmap)
DO_VMSTAT=1            	# turn on/off vmstat 1 "STATS_SAMPLES"
DO_SAR=0              	# turn on/off sar (Solaris: Page-Out and Memory)
DO_NETSTAT=1           	# turn on/off netstat -a
DO_MPSTAT=0           	# turn on/off mpstat 1 "STATS_SAMPLES" (Solaris)
DO_IOSTAT=1            	# turn on/off iostat -xn 1 "STATS_SAMPLES"
STATS_SAMPLES=3         # run *stat/sar for "n" sec 
DO_TRUSS=0            	# turn on/off truss -leafdD for "TRUSS_INTERVAL" secs
DO_TRUSS_C=0            # turn on/off truss -c for "TRUSS_INTERVAL" secs
TRUSS_INTERVAL=5	# run truss for "TRUSS_INTERVAL" sec 
DO_STRACE=1		# turn on/off strace /*M*/
DO_STRACE_C=0	        # turn on/off strace -c for number of seconds
DO_PS=1                	# turn on/off ps /*M*/
DO_ULIMIT=1             # Gather ulimit and proc info before looping /*M*/
DO_LSOF=1		# Gather lsof information /*M*/

# Initialization

sleep_interval=`expr $SLEEP_INTERVAL`
truss_interval=`expr $TRUSS_INTERVAL`
stats_samples=`expr $STATS_SAMPLES`
loop_count=`expr $LOOP_COUNT`
loop=`expr 0`
s_hostname=`uname -a | awk '{print $2}'`
c_datetime=`date +%m%d%y_%H%M%S`
release=`uname -r`

if [ $PTRACE_SCOPE = "1" ]; then
echo "Changing ptrace_scope..."
echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
fi

if [ $release = "5.10" ]; then
    gcorecmd="gcore -c all"
else
    gcorecmd="gcore"
fi

# retrieve process id from first argument in the command line
pid=$1
#  Below is a script to capture LOOP_COUNT sets of data
#  during a JVM "hang/high cpu usage". The script takes a pid as argument.

# capture system configuration data before the loop

if [ -f /usr/bin/lshw ];
then
  echo `date` >> $outlib/lshw.out.$s_hostname.$c_datetime.$pid
  /usr/bin/lshw  >> $outlib/lshw.out.$s_hostname.$c_datetime.$pid
  echo `uname -a` >> $outlib/lshw.out.$s_hostname.$c_datetime.$pid
fi

if [ -f /sbin/lspci ];
then
  echo `date` >> $outlib/lspci.out.$s_hostname.$c_datetime.$pid
  /sbin/lspci >> $outlib/lspci.out.$s_hostname.$c_datetime.$pid
  echo `uname -a` >> $outlib/lspci.out.$s_hostname.$c_datetime.$pid
fi

if [ -f /proc/cpuinfo ]; then
  echo `date` >> $outlib/cpuinfo.out.$s_hostname.$c_datetime.$pid
  cat /proc/cpuinfo >> $outlib/cpuinfo.out.$s_hostname.$c_datetime.$pid
fi

  echo `date` >> $outlib/pargs.out.$s_hostname.$c_datetime.$pid
  echo `uname -a` >> $outlib/pargs.out.$s_hostname.$c_datetime.$pid
  /bin/ps eww -p $pid >> $outlib/pargs.out.$s_hostname.$c_datetime.$pid
  
# capture jinfo data before the loop
if [ $DO_JINFO = 1 ]; then
  echo `date` >> $outlib/jinfo.out.$s_hostname.$c_datetime.$pid
  jinfo $opt $pid >> $outlib/jinfo.out.$s_hostname.$c_datetime.$pid
fi

# capture one core dump of java process at the beginning
if [ $DO_GCORE_BEFORE_LOOP = 1 ]; then
  echo $gcorecmd -o $outlib/core.$s_hostname.$c_datetime.1 $pid
  $gcorecmd -o $outlib/core.$s_hostname.$c_datetime.1 $pid
fi

  # capture address space info of java process - no reason to loop this

  if [ $DO_PMAP = 1 ]; then
    echo `date` >> $outlib/pmap.out.$s_hostname.$c_datetime.$pid
    pmap $pid >> $outlib/pmap.out.$s_hostname.$c_datetime.$pid
    echo "***now again with pmap -x ***" >> $outlib/pmap.out.$s_hostname.$c_datetime.$pid
    pmap -x $pid >> $outlib/pmap.out.$s_hostname.$c_datetime.$pid
  fi

  if [ $DO_ULIMIT = 1 ]; then
    echo `date` >> $outlib/ulimit.out.$s_hostname.$c_datetime.$pid
    ulimit -a >> $outlib/ulimit.out.$s_hostname.$c_datetime.$pid
    echo "*** Proc Info ***" >> $outlib/ulimit.out.$s_hostname.$c_datetime.$pid
    cat /proc/$pid/limits >> $outlib/ulimit.out.$s_hostname.$c_datetime.$pid
    echo "*** Number of open FDs from /proc/pid/fd/ ***" >> $outlib/ulimit.out.$s_hostname.$c_datetime.$pid
    command ls -la /proc/$pid/fd/* | wc -l >> $outlib/ulimit.out.$s_hostname.$c_datetime.$pid; 
  fi

  if [ $DO_LSOF = 1 ]; then
    echo `date` >> $outlib/lsof.out.$s_hostname.$c_datetime.$pid
    lsof -a -p $pid >> $outlib/lsof.out.$s_hostname.$c_datetime.$pid
  fi


# repeat the following data capture loop "LOOP_COUNT" times

while [ $loop -lt $loop_count ]
do
    loop=`expr $loop + 1`
    #echo "Loop " $loop

  # 1. capture top cpu usage on both java process and the system

  if [ $DO_PRSTAT = 1 ]; then

    echo `date` >> $outlib/prstat.out.$s_hostname.$c_datetime.$pid.$loop
    prstat -L 1 1 >> $outlib/prstat.out.$s_hostname.$c_datetime.$pid.$loop
    prstat -L -p $pid 1 1 >> $outlib/prstat.out.$s_hostname.$c_datetime.$pid.$loop
    prstat -m 1 1 >> $outlib/prstat.out.$s_hostname.$c_datetime.$pid.$loop
  fi

if [ $DO_TOP = 1 ]; then
   echo `date` >> $outlib/top.out.$s_hostname.$c_datetime.$pid.$loop
   top -n 1 -H -p $pid >> $outlib/top.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 2. capture native thread stack of java process

  if [ $DO_PSTACK = 1 ]; then
    echo `date` >> $outlib/pstack.out.$s_hostname.$c_datetime.$pid.$loop
    pstack $pid >> $outlib/pstack.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 3. capture java thread stack of java process

  if [ $DO_JSTACK = 1 ]; then
    echo `date` >> $outlib/jstack.out.$s_hostname.$c_datetime.$pid.$loop
    jstack $opt -m -l $pid >> $outlib/jstack.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  if [ $DO_KILL3 = 1 ]; then
    # *NOTE* that java thread dump output goes to "stdout" of the pid
    kill -3 $pid
  fi

  # 4. capture heap summary, jmap is available starting JDK 1.4.2_09

  if [ $DO_HEAPSUM = 1 ]; then
    echo `date` >> $outlib/heapsum.out.$s_hostname.$c_datetime.$pid.$loop
    jmap $opt -heap $pid >> $outlib/heapsum.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 5. vmstat
  if [ $DO_VMSTAT = 1 ]; then
    echo `date` >> $outlib/vmstat.out.$s_hostname.$c_datetime.$pid.$loop
    vmstat 1 $stats_samples >> $outlib/vmstat.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 6. sar for monitoring paging activities
  if [ $DO_SAR = 1 ]; then
    echo `date` >> $outlib/sar.out.$s_hostname.$c_datetime.$pid.$loop
    sar -g 1 $stats_samples >> $outlib/sar.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 7. netstat 

  if [ $DO_NETSTAT = 1 ]; then
    echo `date` >> $outlib/netstat.out.$s_hostname.$c_datetime.$pid.$loop
    netstat -a >> $outlib/netstat.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 8. mpstat 

  if [ $DO_MPSTAT = 1 ]; then
    echo `date` >> $outlib/mpstat.out.$s_hostname.$c_datetime.$pid.$loop
    mpstat 1 $stats_samples >> $outlib/mpstat.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 9. iostat

  if [ $DO_IOSTAT = 1 ]; then
    echo `date` >> $outlib/iostat.out.$s_hostname.$c_datetime.$pid.$loop
    iostat -xn 1 $stats_samples >> $outlib/iostat.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # 10. truss -leafdD -p <pid>

  if [ $DO_TRUSS = 1 ]; then
    truss -o $outlib/truss.out.$s_hostname.$c_datetime.$pid.$loop -leafdD -p $pid &
    truss_pid=$!
    sleep $truss_interval

    echo "Trussing pid " $truss_pid

    kill -9 $truss_pid
  fi

  # 11. truss -c -p <pid>

  if [ $DO_TRUSS_C = 1 ]; then

    truss -o $outlib/truss_c.out.$s_hostname.$c_datetime.$pid.$loop -c -p $pid &
    trussc_pid=$!

    echo "Trussing -c pid " $trussc_pid

    sleep $truss_interval

    kill $trussc_pid
  fi

  # 12. strace
 if [ $DO_STRACE = 1 ]; then
    strace -o $outlib/strace.out.$s_hostname.$c_datetime.$pid.$loop -FfqrT -p $pid &
    strace_pid=$!
    sleep $truss_interval

    echo "stracing pid " $strace_pid

    kill -9 $strace_pid
  fi

  # 13. strace -c
if [ $DO_STRACE_C = 1 ]; then

    strace -o $outlib/strace_c.out.$s_hostname.$c_datetime.$pid.$loop -c -p $pid &
    stracec_pid=$!

    echo "Strace -c pid " $stracec_pid

    sleep $truss_interval

    kill $stracec_pid
  fi

  # 14. ps -Llefy

  if [ $DO_PS = 1 ]; then
    echo `date` >> $outlib/ps.out.$s_hostname.$c_datetime.$pid.$loop
    ps -Llefy >> $outlib/ps.out.$s_hostname.$c_datetime.$pid.$loop
  fi

  # sleep for a few seconds before collecting next set of data
  #echo sleep $sleep_interval
  sleep $sleep_interval

done


# capture one more core dump of java process at the end
if [ $DO_GCORE_AFTER_LOOP = 1 ]; then
  echo $gcorecmd -o $outlib/core.$s_hostname.$c_datetime.2 $pid
  $gcorecmd -o $outlib/core.$s_hostname.$c_datetime.2 $pid
fi

# capture heap dump of java process at the end
if [ $DO_HEAPDUMP_AFTER_LOOP = 1 ]; then
  echo jmap -heap:format=b $pid
  jmap -heap:format=b $pid
  mv heap.bin $outlib/heap.bin.$s_hostname.$c_datetime
fi

echo "Creating tar ball......"
tar cvf $s_hostname.$c_datetime.tar $outlib

echo "Starting tar file compression........."
gzip $s_hostname.$c_datetime.tar

echo "Data collection script complete......"
echo "The output file is currently named $s_hostname.$c_datetime.tar.gz"
echo "Please prepend your CaseID # to $s_hostname.$c_datetime.tar.gz and send it to Support."
